class TestBugF2 {
    public static void main(String[] a) {
        System.out.println(new Test().f());
    }
}

class Test {
    //The function called from main.
    public int f() {
        boolean result;
        
        //Placing a function call on the right hand side of "||" that prints out an extra "1"
        //and making the left hand side true to check the bug where it 
        //evaluates the right hand side even if left hand side is true.
        //The function on the right should not be called when
        //the program is compiled using a correct compiler.
        result =  true || this.g(1);
	    return 0;
	}

    //Function prints out the passed value when called
	public boolean g(int n) {
        System.out.println(n);
        return true;
    }
}

