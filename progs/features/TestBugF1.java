class TestBugF1 {
    public static void main(String[] a) {
		if (new Test().f()) {} else {}
    }
}

class Test {
	//The function called from main.
    public boolean f() {
		boolean result;

		//Placing integers on both sides of || to check the bug where it 
		//allows ORing of integers. Correct compiler should give a type error.
		result = 0 || 1;	//ORing of integers

		if (result){
			System.out.println(1);	//Print 1 if evaluated to true
		}
		else System.out.println(0);	//Print 0 if evaluated to false
	return result;
	}
}

