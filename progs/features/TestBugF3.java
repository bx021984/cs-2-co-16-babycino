class TestBugF3 {
    public static void main(String[] a) {
		if (new Test().f()) {} else {}
    }
}

class Test {
	//The function called from main.
    public boolean f() {
		boolean result;

		//Making the left hand side false and right hand side true to check
		//the bug where the result is false if the left hand side is false even if
		//the right hand side is true. When the program is compiled using a correct
		//compiler, this should evaluate to true.
		result = false || true;

		if (result){
			System.out.println(1);	//Print 1 if evaluated to true
		}
		else System.out.println(0);	//Print 0 if evaluated to false
	return result;
	}
}

